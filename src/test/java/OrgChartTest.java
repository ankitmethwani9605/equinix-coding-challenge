import Models.Employee;
import Models.Organization;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class OrgChartTest extends TestCase {

    private Organization organization;
    private OrgChart orgChart;
    private static Boolean setUpIsDone = false;

    @Before
    public void setUp() {
        if (setUpIsDone) {
            return;
        }
        orgChart = new OrgChart();
        organization = Organization.getInstance();
        orgChart.readPersonalFile();
        orgChart.readOrganizationFile();
        orgChart.readTeamFile();
        setUpIsDone = true;
    }

    @Test
    public void testCheckIfPersonPresent() {
        orgChart = new OrgChart();
        organization = Organization.getInstance();

        ArrayList<Integer> expectedPersonIds = new ArrayList<>();
        expectedPersonIds.add(1000);

        List<Employee> employeesList = organization.findPersonByName("John");

        assert employeesList.size() == 1;
        for (int i = 0; i < employeesList.size(); i++) {
            int observedPersonId = employeesList.get(i).getPersonId();
            int expectedPersonId = expectedPersonIds.get(i);
            assert observedPersonId == expectedPersonId;
        }
    }

    @Test
    public void testCheckIfPersonNotPresent() {
        orgChart = new OrgChart();
        organization = Organization.getInstance();

        List<Employee> employeesList = organization.findPersonByName("abc");

        assert employeesList.size() == 0;
    }

    // Happy Case
    @Test
    public void testCheckByFirstName() {
        orgChart = new OrgChart();
        organization = Organization.getInstance();

        ArrayList<Integer> expectedPersonIds = new ArrayList<>();
        expectedPersonIds.add(1000);
        expectedPersonIds.add(1001);
        expectedPersonIds.add(1002);

        List<Employee> employeesList = organization.findPersonByName("John");
        List<Employee> empList = new ArrayList<>();
        for (Employee emp : employeesList) {
            Queue<Employee> queue = new LinkedList<>();
            queue.add(emp);
            empList.addAll(orgChart.findEmployees(queue, 2));
        }
        assert empList.size() == 3;
        for (int i = 0; i < empList.size(); i++) {
            int observedPersonId = empList.get(i).getPersonId();
            int expectedPersonId = expectedPersonIds.get(i);
            assert observedPersonId == expectedPersonId;
        }
    }

    // Happy Case
    @Test
    public void testCheckByLastName() {
        orgChart = new OrgChart();
        organization = Organization.getInstance();

        ArrayList<Integer> expectedPersonIds = new ArrayList<>();
        expectedPersonIds.add(1000);
        List<Employee> employeesList = organization.findPersonByName("Doe");
        List<Employee> empList = new ArrayList<>();
        for (Employee emp2 : employeesList) {
            Queue<Employee> q = new LinkedList<>();
            q.add(emp2);
            empList.addAll(orgChart.findEmployees(q, 1));
        }

        assert empList.size() == 1;
        for (int i = 0; i < empList.size(); i++) {
            int observedPersonId = empList.get(i).getPersonId();
            int expectedPersonId = expectedPersonIds.get(i);
            assert observedPersonId == expectedPersonId;
        }
    }

    // Happy Case
    @Test
    public void testCheckByFirstAndLastName() {
        orgChart = new OrgChart();
        organization = Organization.getInstance();

        ArrayList<Integer> expectedPersonIds = new ArrayList<>();
        expectedPersonIds.add(1000);
        expectedPersonIds.add(1001);
        expectedPersonIds.add(1002);
        List<Employee> employeesList = organization.findPersonByName("John Doe");
        List<Employee> empList = new ArrayList<>();
        for (Employee emp2 : employeesList) {
            Queue<Employee> q = new LinkedList<>();
            q.add(emp2);
            empList.addAll(orgChart.findEmployees(q, 2));
        }

        assert empList.size() == 3;
        for (int i = 0; i < empList.size(); i++) {
            int observedPersonId = empList.get(i).getPersonId();
            int expectedPersonId = expectedPersonIds.get(i);
            assert observedPersonId == expectedPersonId;
        }
    }
}