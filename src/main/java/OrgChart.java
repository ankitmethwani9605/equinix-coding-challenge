import Models.Address;
import Models.Organization;
import Models.Employee;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class OrgChart {

    private static String BASEPATH = "resources/";
    private static String PERSONALDATAFILENAME = "personal.txt";
    private static String ORGANIZATIONDATAFILENAME = "organization.txt";
    private static String TEAMDATAFILENAME = "team.txt";
    private BufferedReader br = null;
    private FileInputStream fileInputStream = null;

    private void readFile(String fileName) {
        try {
            fileInputStream = new FileInputStream(BASEPATH + fileName);
            br = new BufferedReader(new InputStreamReader(fileInputStream));
            br.readLine(); //read the headers to avoid adding them to objects
        } catch (Exception e) {
            System.err.println("Error occurred while reading file");
            e.printStackTrace();
        }
    }

    private void closeFile() {
        try {
            fileInputStream.close();
            br.close();
        } catch (Exception e) {
            System.err.println("Error occurred while closing file");
            e.printStackTrace();
        }
    }

    public void readPersonalFile() {
        Organization organization = Organization.getInstance();
        try {
            readFile(PERSONALDATAFILENAME);
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] data = strLine.split(", ");

                Address address = new Address();
                String streetAddress = data[3];
                String[] street = streetAddress.split(" ");
                address.setStreetNo(street[0]);
                StringBuilder streetName = new StringBuilder();
                for (int i = 1; i < street.length; i++) {
                    streetName.append(street[i]);
                }
                address.setStreetName(streetName.toString());
                address.setCity(data[4]);
                address.setState(data[5]);

                Employee employee = new Employee.EmployeeBuilder(data[0], data[1], Integer.parseInt(data[6]))
                        .phone(data[2])
                        .address(address)
                        .build();

                organization.addPerson(employee);
            }
            closeFile();
        } catch (Exception e) {
            System.err.println("Error occurred in readPersonFile method");
            e.printStackTrace();
        }
    }

    public void readOrganizationFile() {
        Organization organization = Organization.getInstance();
        try {
            readFile(ORGANIZATIONDATAFILENAME);
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] data = strLine.split(", ");
                int personId = Integer.parseInt(data[2]);
                Employee employee = organization.findPersonById(personId);
                if (employee == null) {
                    throw new Exception("Invalid data");
                }
                employee.setRole(data[0]);
                employee.setTeam(data[1]);
            }
            closeFile();
        } catch (Exception e) {
            System.err.println("Error occurred in readOrganizationFile method");
            e.printStackTrace();
        }
    }

    public void readTeamFile() {
        Organization organization = Organization.getInstance();
        try {
            readFile(TEAMDATAFILENAME);
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] data = strLine.split(", ");
                int personId = Integer.parseInt(data[0]);
                Employee person = organization.findPersonById(personId);
                if (person == null) {
                    throw new Exception("Invalid person id (col 1)");
                }
                int empId = Integer.parseInt(data[1]);
                Employee employee = organization.findPersonById(empId);
                if (employee == null) {
                    throw new Exception("Invalid employee id (col 2)");
                }
                person.addEmployee(employee);
            }
            closeFile();
        } catch (Exception e) {
            System.err.println("Error occurred in readTeamFile method");
            e.printStackTrace();
        }
    }

    public List<Employee> findEmployees(Queue<Employee> queue, int level) {
        List<Employee> emps = new ArrayList<>();
        int currLevel = 0;
        while (!queue.isEmpty() && currLevel < level) {
            int n = queue.size();
            for (int i = 0; i < n; i++) {
                Employee employee = queue.poll();
                assert employee != null;
                emps.add(employee);
                List<Employee> employees = employee.getManages();
                for (Employee emp : employees) {
                    queue.add(emp);
                }
            }
            currLevel++;
        }

        return emps;
    }

    private void printEmployees(List<Employee> employees) {
        System.out.println("First name | Last Name | Tile | Organization | Phone No.");
        System.out.println("-----------------------------------------------------------");
        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

    private boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        try {
            OrgChart orgChart = new OrgChart();
            Organization organization = Organization.getInstance();

            orgChart.readPersonalFile();
            orgChart.readOrganizationFile();
            orgChart.readTeamFile();

            if (args.length == 0) {
                throw new Exception("Command Line arguments not provided");
            }

            String queryName = "";
            if (args.length >= 2 && !orgChart.isInt(args[1])) {
                queryName = args[0] + " " + args[1];
            } else {
                queryName = args[0];
            }

            int level = 3;
            if (args.length == 2 && orgChart.isInt(args[1])) {
                level = Integer.parseInt(args[1]);
            }
            if (args.length == 3 && orgChart.isInt(args[2])) {
                level = Integer.parseInt(args[2]);
            }

            List<Employee> employees = organization.findPersonByName(queryName);
            List<Employee> allEmployees = new ArrayList<>();
            if (employees.size() == 0) {
                throw new Exception("No such employee found");
            }
            for (Employee employee : employees) {
                Queue<Employee> queue = new LinkedList<>();
                queue.add(employee);
                allEmployees.addAll(orgChart.findEmployees(queue, level));
            }
            orgChart.printEmployees(allEmployees);


        } catch (Exception e) {
            System.err.println("Error occurred in main method");
            e.printStackTrace();
        }
    }
}
