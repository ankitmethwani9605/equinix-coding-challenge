package Models;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    String firstName;
    String lastName;
    String phone;
    Address address;
    int personId;
    String team;
    String role;
    List<Employee> manages;


    private Employee(EmployeeBuilder builder) {
        manages = new ArrayList<>();
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.phone = builder.phone;
        this.address = builder.address;
        this.personId = builder.personId;
        this.team = builder.team;
        this.role = builder.role;

    }

    public List<Employee> getManages() {
        return manages;
    }

    public void addEmployee(Employee employee) {
        this.manages.add(employee);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return this.getFirstName() + " | " + this.getLastName() + " | " + this.getRole() + " | " + this.getTeam() + " | " + this.getPhone() ;
    }


    public static class EmployeeBuilder {
        private String firstName;
        private String lastName;
        private String phone;
        private Address address;
        private int personId;
        private String team;
        private String role;

        public EmployeeBuilder(String firstName, String lastName, int personId) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.personId = personId;
        }

        public Employee build() {
            Employee employee = new Employee(this);
            return employee;
        }

        public EmployeeBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public EmployeeBuilder address(Address address) {
            this.address = address;
            return this;
        }

        public EmployeeBuilder team(Address address) {
            this.address = address;
            return this;
        }

        public EmployeeBuilder role(String role) {
            this.role = role;
            return this;
        }

    }
}
