package Models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Organization {

    private final HashMap<Integer, Employee> idMap = new HashMap<Integer, Employee>();
    private final HashMap<String, List<Integer>> nameMap = new HashMap<String, List<Integer>>();
    private static Organization instance = null;

    private Organization() {

    }

    public static Organization getInstance() {
        if (instance == null) {
            instance = new Organization();
        }
        return instance;
    }

    public void addPerson(Employee employee) {
        idMap.put(employee.getPersonId(), employee);
        nameMap.computeIfAbsent(employee.getFirstName(), k -> new ArrayList<>());
        nameMap.computeIfAbsent(employee.getLastName(), k -> new ArrayList<>());
        nameMap.computeIfAbsent(employee.getFirstName() + " " + employee.getLastName(), k -> new ArrayList<>());
        nameMap.get(employee.getFirstName()).add(employee.getPersonId()); // adding by first Name
        nameMap.get(employee.getLastName()).add(employee.getPersonId()); // adding by last Name
        nameMap.get(employee.getFirstName() + " " + employee.getLastName()).add(employee.getPersonId()); // adding by first name, last name combination
    }

    public Employee findPersonById(int personId) {
        if (idMap.containsKey(personId)) {
            return idMap.get(personId);
        }
        return null;
    }

    public List<Employee> findPersonByName(String name) {
        List<Employee> employees = new ArrayList<Employee>();
        if (nameMap.containsKey(name)) {
            for (int id : nameMap.get(name)) {
                employees.add(idMap.get(id));
            }
        }
        return employees;
    }

}
