## Equinix Coding Challenge

**Assumptions:**

* There are three separate text files viz. personal.txt, org.text, team.txt
* All the data in all three text files should be consistent.
* Each text files are comma separated (eg. FirstName, LastName, PhoneNo, StreetNo, StreetName, City, State, PersonID)
* If level is not specified in the command line, I have considered as 3
* if we are searching for the "firstname/ lastname" in the orgchart that are present multiple times then I have listed org chart for all the cases.

**Class Diagram:**

![Scheme](https://gitlab.com/ankitmethwani9605/equinix-coding-challenge/-/raw/master/ClassDiagram.jpeg)

**Apporach:**

* Loaded all the data from personal.txt, org.text, team.txt
* Created Hashmap to map firstname, lastname and (firstname and lastname) to respective ids eg. 'john' - > [1003,1000] if there are multiple 'john' in the data, similarly for lastname and (firstname and lastname)  
* Now if only name is given in the command line
	1. Search name in the map, if the name is not present return throw exception.
	2. If name is present, add name in the list and add all the employees that the current employees manages.
	3. After adding all the employee object lastly print it by removing each and everyone in FIFO fashion.
* Now if name and level(x) is given in the command line
	1. Search name in the map, if the name is not present return throw exception.
	2. If name is present, add name in the list and add all the employees that the current employees manages uptil 'x' levels.
	3. After adding all the employee object lastly print it by removing each and everyone in FIFO fashion.

**Unit Testing:**

* Checked if the employee that we are searching for is present.
* Checked if the employee that we are searching for is not present.
* Checked if code works if only firstname is given
* Checked if code works if only lastname is given
* Checked if code works if only firstname & lastname is given

**Design pattern used:**

* Builder pattern
* Singleton pattern
* Composite pattern